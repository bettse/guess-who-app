require('dotenv').config()
const electron = require('electron')
const settings = require('electron-settings')
const path = require('path')
const url = require('url')
const { RtmClient, WebClient, CLIENT_EVENTS, RTM_EVENTS } = require('@slack/client')
const menubar = require('menubar')
const { URL } = require('url')

const startUrl = process.env.ELECTRON_START_URL || url.format({
  pathname: path.join(__dirname, '/index.html'),
  protocol: 'file:',
  slashes: true
})

const { ipcMain } = electron
const dir = path.join(__dirname, '/')

const mb = menubar({
  dir,
  width: 1024,
  height: 768,
  preloadWindow: true
})

let rtm, web
const userCache = {}
const channelCache = {}

mb.on('after-create-window', afterCreateWindow)
mb.on('after-show', afterShow)
mb.on('ready', () => {
  mb.app.dock.setIcon(dir + 'icon.png')
})

function afterCreateWindow () {
  mb.window.webContents.openDevTools()
  mb.window.webContents.on('did-navigate', didNavigate)

  if (process.env.SLACK_TOKEN) {
    settings.set('slack.access_token', process.env.SLACK_TOKEN)
  }

  if (settings.has('slack.access_token')) {
    enableRTM()
  }
}

function didNavigate (event, url) {
  const myURL = new URL(url)
  if (myURL.searchParams.has('access_token')) {
    const accessToken = myURL.searchParams.get('access_token')
    settings.set('slack.access_token', accessToken)
    enableRTM()
    // TODO: Navigate back to startUrl to remove access_token from url
    // mb.window.webContents.loadURL(startUrl)
  }
}

function afterShow () {
  if (settings.has('slack.connectData')) {
    const connectData = settings.get('slack.connectData')
    mb.window.webContents.send('AUTHENTICATED', connectData)
  } else {
    mb.window.webContents.send('SLACK_CLIENT_ID', process.env.SLACK_CLIENT_ID)
  }
}

ipcMain.on('logout', (event, code) => {
  settings.delete('slack.access_token')
  settings.delete('slack.connectData')
  mb.window.webContents.send('AUTHENTICATED', null)
  rtm.disconnect()
  rtm = null
})

function enableRTM (cb) {
  if (rtm) {
    console.log('rtm already set')
    return
  }
  const accessToken = settings.get('slack.access_token')
  rtm = new RtmClient(accessToken, {
    dataStore: false,
    useRtmConnect: true
  })

  rtm.on(CLIENT_EVENTS.RTM.AUTHENTICATED, authenticated)
  rtm.on(RTM_EVENTS.USER_TYPING, userTyping)
  rtm.start()
}

ipcMain.on('fake', (event, arg) => userTyping(arg))

function userTyping (event) {
  event.user = userCache[event.user] || event.user
  event.channel = channelCache[event.channel] || event.channel
  mb.window.webContents.send('USER_TYPING', event)
}

function authenticated (connectData) {
  settings.set('slack.connectData', connectData)
  web = new WebClient(settings.get('slack.access_token'))

  web.channels.list().then(cacheChannels('channels')).catch(console.error)
  web.groups.list().then(cacheChannels('groups')).catch(console.error)
  web.im.list().then(cacheChannels('ims')).catch(console.error)

  web.users.list().then((res) => { res.members.forEach((m) => userCache[m.id] = m) }).catch(console.error).then(() => console.log(`${Object.keys(userCache).length} users`))
  mb.window.webContents.send('AUTHENTICATED', connectData)
}

function cacheChannels(key) {
  return function(res) {
    console.log(`${res[key].length} ${key}`)
    res[key].forEach((x) => channelCache[x.id] = x)
  }
}
