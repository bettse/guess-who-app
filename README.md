# Guess Who

Find builds at https://bettse.gitlab.io/guess-who-app/

![Guess who icon](./public/icon.png)

This is an electron menu bar app for MacOS to create notifications anytime you get a typing notification in slack. Clicking the icon in the menu bar will display a configuration panel showing status and allowing toggline off notifications for public channels, private channels, direct messages, and multi-person direct messages.

At one time I had a web server backend that could be used to authorize this, but it was decomishioned because the Slack instance I used this with didn't allow users to add new apps.

Instead I set a legacy slack token as an environment variable before launching:

`launchctl setenv SLACK_TOKEN xoxp-...`

You can also edit `environment.plist` and copy to `~/Library/LaunchAgents/`
