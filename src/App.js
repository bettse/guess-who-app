import React, { Component } from 'react'
import { Button, FormGroup, Checkbox } from 'react-bootstrap'
import logo from './logo.png'
import './App.css'
const electron = window.require('electron')

const { ipcRenderer } = electron
const { URLSearchParams } = window

class App extends Component {
  constructor (props) {
    super(props)
    this.state = {
      clientId: '',
      connectData: null,
      events: [],
      options: {
        privateChannels: true,
        publicChannels: true,
        privateMessages: true,
        multipersonMessages: true
      }
    }

    ipcRenderer.on('SLACK_CLIENT_ID', (event, arg) => {
      console.log('SLACK_CLIENT_ID', arg)
      this.setState({clientId: arg})
    })

    ipcRenderer.on('AUTHENTICATED', this.login.bind(this))
    ipcRenderer.on('USER_TYPING', this.dispatchNotifications.bind(this))
  }

  login (event, connectData) {
    const currentConnectData = this.state.connectData
    if ( !currentConnectData || currentConnectData.self.id !== connectData.self.id) {
      console.log('AUTHENTICATED', connectData)
      new Notification('Authenticated', {
        silent: true,
        body: `Logged in to ${connectData.team.name} as ${connectData.self.name}`
      })
      this.setState({connectData})
    }
  }

  logout () {
    ipcRenderer.send('logout')
  }

	dispatchNotifications (_, event) {
		console.log('USER_TYPING', event)
    const { options } = this.state
    const { user, channel } = event

    const showMPIM = options.multipersonMessages && channel.is_mpim
    const showPrivateChannel = options.privateChannels && ((channel.is_private && channel.is_channel) || channel.is_group)
    const showPrivateMessage = options.privateMessages && ((channel.is_private && !channel.is_channel) || channel.is_im)
    const showPublicChannel = options.publicChannels && (!channel.is_private && channel.is_channel)

		if (showMPIM || showPrivateChannel || showPublicChannel) {
      new Notification('Typing', {
        silent: true,
        icon: user.profile.image_512,
        body: `${user.name} is typing in ${channel.name}`
      })
    } else if (showPrivateMessage) {
      new Notification('Typing', {
        silent: true,
        icon: user.profile.image_512,
        body: `${user.name} is typing an IM`
      })
    } else {
      console.log("Not showing typing event")
    }
	}

  componentDidMount () {
    // Oauth callback
    const urlParams = new URLSearchParams(window.location.search)
    if (urlParams.has('code')) {
      ipcRenderer.send('code', urlParams.get('code'))
    }
  }

  renderSignIn () {
    const { clientId } = this.state
    return (
        <div className='App'>
          <header className='App-header'>
            <img src={logo} className='App-logo' alt='logo' />
            <h1 className='App-title'>Welcome to React</h1>
          </header>
          <p className='App-intro'>
            {clientId &&
              <a href={`https://slack.com/oauth/authorize?scope=client&client_id=${clientId}&state=${window.location.href}`}>
                <img alt='Add to Slack'
                  height='40' width='139'
                  src='https://platform.slack-edge.com/img/add_to_slack.png'
                  srcSet='https://platform.slack-edge.com/img/add_to_slack.png 1x, https://platform.slack-edge.com/img/add_to_slack@2x.png 2x'
                />
              </a>
            }
          </p>
        </div>
      )
  }

  render () {
    const { connectData } = this.state
    if (connectData) {
      return this.renderOptions()
    } else {
      return this.renderSignIn()
    }
  }

  renderOptions () {
    const { connectData, options } = this.state
    return (
      <div className='App'>
        <header className='App-header'>
          <img src={logo} className='App-logo' alt='logo' />
          <h1 className='App-title'>{connectData.self.name} at {connectData.team.name}</h1>
        </header>
        <Button bsStyle="primary" bsSize="large" onClick={this.sendFakeEvent.bind(this)}>Fake Event</Button>
        <form>
          <FormGroup>
            <Checkbox defaultChecked={options.publicChannels} name="publicChannels" onChange={this.toggleOption.bind(this)}>Public Channels</Checkbox>
            <Checkbox defaultChecked={options.privateChannels} name="privateChannels" onChange={this.toggleOption.bind(this)}>Private Channels</Checkbox>
            <Checkbox defaultChecked={options.privateMessages} name="privateMessages" onChange={this.toggleOption.bind(this)}>Private Messages</Checkbox>
            <Checkbox defaultChecked={options.multipersonMessages} name="multipersonMessages" onChange={this.toggleOption.bind(this)}>Multiperson Messages</Checkbox>
          </FormGroup>
        </form>
        <p onClick={this.logout}>Logout</p>
      </div>
    )
  }

  toggleOption(event) {
    const target = event.target
    const value = target.type === 'checkbox' ? target.checked : target.value
    const name = target.name
    const { options } = this.state
		options[name] = value

    this.setState({ options })
  }

  sendFakeEvent() {
    console.log('Sending fake typing event')
    const event = {
      "type": "user_typing",
      "channel": "C1R4YR6Q7",
      "user": "U1S5T7BDL"
    }
    ipcRenderer.send('fake', event)
  }
}

export default App
